package sqlog

import (
	"context"
	"database/sql"
	"log"
	"time"
)

type DB struct {
	*sql.DB
	prefix      string
	logDuration bool
}

func Wrap(db *sql.DB) *DB {
	return &DB{DB: db}
}

func (db *DB) LogDuration(logDuration bool) *DB {
	db.logDuration = logDuration
	return db
}

func (db *DB) Prefix(prefix string) *DB {
	db.prefix = prefix
	return db
}

func (db *DB) Exec(query string, args ...interface{}) (sql.Result, error) {
	if db.logDuration {
		start := time.Now()
		res, err := db.DB.Exec(query, args...)
		log.Printf("DB: %s (%s) %v", db.prefix, query, time.Now().Sub(start))
		return res, err
	}
	log.Printf("DB: %s (%s)", db.prefix, query)
	return db.DB.Exec(query, args...)
}

func (db *DB) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	if db.logDuration {
		start := time.Now()
		res, err := db.DB.ExecContext(ctx, query, args...)
		log.Printf("DB: %s (%s) %v", db.prefix, query, time.Now().Sub(start))
		return res, err
	}
	log.Printf("DB: %s (%s)", db.prefix, query)
	return db.DB.ExecContext(ctx, query, args...)
}

func (db *DB) Query(query string, args ...interface{}) (*sql.Rows, error) {
	if db.logDuration {
		start := time.Now()
		res, err := db.DB.Query(query, args...)
		log.Printf("DB: %s (%s) %v", db.prefix, query, time.Now().Sub(start))
		return res, err
	}
	log.Printf("DB: %s (%s)", db.prefix, query)
	return db.DB.Query(query, args...)
}

func (db *DB) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	if db.logDuration {
		start := time.Now()
		res, err := db.DB.QueryContext(ctx, query, args...)
		log.Printf("DB: %s (%s) %v", db.prefix, query, time.Now().Sub(start))
		return res, err
	}
	log.Printf("DB: %s (%s)", db.prefix, query)
	return db.DB.QueryContext(ctx, query, args...)
}

func (db *DB) QueryRow(query string, args ...interface{}) *sql.Row {
	if db.logDuration {
		start := time.Now()
		res := db.DB.QueryRow(query, args...)
		log.Printf("DB: %s (%s) %v", db.prefix, query, time.Now().Sub(start))
		return res
	}
	log.Printf("DB: %s (%s)", db.prefix, query)
	return db.DB.QueryRow(query, args...)
}

func (db *DB) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	if db.logDuration {
		start := time.Now()
		res := db.DB.QueryRowContext(ctx, query, args...)
		log.Printf("DB: %s (%s) %v", db.prefix, query, time.Now().Sub(start))
		return res
	}
	log.Printf("DB: %s (%s)", db.prefix, query)
	return db.DB.QueryRowContext(ctx, query, args...)
}
